# Time Reports for JIRA cloud #
This site contains the issue tracker and wiki for Time Reports for JIRA cloud.

Go to the [issue tracker](https://bitbucket.org/smoothicsdev/jiratimereports/issues?status=new&status=open "Issue tracker") to report bugs or request features.

Go to the [wiki](https://bitbucket.org/smoothicsdev/jiratimereports/wiki/ "Wiki") to find further information about this add-on.